﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace AplicacionDemo
{
    public partial class frmDivisionPoliticaAdministrativa : Form
    {
        public frmDivisionPoliticaAdministrativa()
        {
            InitializeComponent();
        }

        private void frmDivisionPoliticaAdministrativa_Load(object sender, EventArgs e)
        {
            string url = "http://apis.modernizacion.cl/dpa/regiones/";
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(responseRegionesComplete);
            wc.DownloadStringAsync(new Uri(url));
        }

        void responseRegionesComplete(object sender, DownloadStringCompletedEventArgs e)
        {
            var json = e.Result;
            dynamic m = JsonConvert.DeserializeObject(json);
            cboRegion.DisplayMember = "nombre";
            cboRegion.ValueMember = "codigo";
            cboRegion.DataSource = m;
            cboRegion.Enabled = true;
        }
        private void cboRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboProvincia.Enabled = false;
            cboComuna.Enabled = false;
            string codigoRegion = cboRegion.SelectedValue.ToString();
            poblarCboProvincia(codigoRegion);
        }

        private void poblarCboProvincia(string codigoRegion)
        {
            string url = "http://apis.modernizacion.cl/dpa/regiones/" + codigoRegion + "/provincias";
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(responseProvinciasComplete);
            wc.DownloadStringAsync(new Uri(url));
        }

        private void responseProvinciasComplete(object sender, DownloadStringCompletedEventArgs e)
        {
            var json = e.Result;
            dynamic m = JsonConvert.DeserializeObject(json);
            cboProvincia.DisplayMember = "nombre";
            cboProvincia.ValueMember = "codigo";
            cboProvincia.DataSource = m;
            cboProvincia.Enabled = true;
        }

        private void cboProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboComuna.Enabled = false;
            string codigoProvincia = cboProvincia.SelectedValue.ToString();
            poblarCboComuna(codigoProvincia);
        }

        private void poblarCboComuna(string codigoProvincia)
        {
            string url = "http://apis.modernizacion.cl/dpa/provincias/" + codigoProvincia + "/comunas";
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(responseComunasComplete);
            wc.DownloadStringAsync(new Uri(url));
        }

        private void responseComunasComplete(object sender, DownloadStringCompletedEventArgs e)
        {
            var json = e.Result;
            dynamic m = JsonConvert.DeserializeObject(json);
            cboComuna.DataSource = m;
            cboComuna.DisplayMember = "nombre";
            cboComuna.ValueMember = "codigo";
            cboComuna.Enabled = true;
        }
    }
}
