﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DemoClienteHttp
{
    public class Comuna
    {
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Url { get; set; }
        public string CodigoPadre { get; set; }
    }
    class Program
    {

        static void Main(string[] args)
        {
            string url = "http://apis.modernizacion.cl/dpa/comunas/";
            var json = new WebClient().DownloadString(url);
            dynamic m = JsonConvert.DeserializeObject(json);
            foreach (var item in m)
            {
                Console.WriteLine(item.nombre);
            }
            Console.Read();
        }
    }
}
